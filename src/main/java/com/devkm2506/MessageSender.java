package com.devkm2506;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public void sendMessageToQueue(final Object object) {
		jmsTemplate.convertAndSend(object);
	}
	
	public Object consumeMessageFromQueue() {
		return jmsTemplate.receiveAndConvert();
	}

}
