package com.devkm2506;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class App {

    private static ApplicationContext context;

	public static void main(final String...args) throws FileNotFoundException, IOException {

        context = new ClassPathXmlApplicationContext("jmsContext.xml");
        
        TransactionProducer producer = (TransactionProducer) context.getBean("transactionProducer");
        //TransactionConsumer consumer = (TransactionConsumer) context.getBean("transactionConsumer");
        Resource resource = new ClassPathResource("player.xml");

        @SuppressWarnings("resource")
		Scanner scanner = new Scanner(new File(resource.getURI())).useDelimiter("\\Z");
        String contents = scanner.next();
        
        //sendDummyMessages(producer);
        producer.sendMessage(contents);
        
        System.out.println("Messages sent to queue");
        
        //System.out.println(consumer.consumeMessage().toString());
	}
	/** 
	private static void sendDummyMessages(TransactionProducer producer) {
		Transaction transactions = 
				new Transaction(
						110423, 
						"ADJPRICE", 
						new Account(
								"ADJPRICE", 
								"ADJPRICE", 
								0, 
								0),
						new Account(
								"INVASSET", 
								"INVASSET", 
								0, 
								0), 
						new Date());
				
		transactions.add(
				new Transaction(
						110428, 
						"CRTINV", 
						new Account(
								"ADJQTY", 
								"ADJQTY", 
								0, 
								100),
						new Account(
								"INVASSET", 
								"INVASSET", 
								100, 
								0), 
						new Date())
				);
		transactions.add(
				new Transaction(
						110423, 
						"ADJPRICE", 
						new Account(
								"ADJPRICE", 
								"ADJPRICE", 
								0, 
								0),
						new Account(
								"INVASSET", 
								"INVASSET", 
								0, 
								0), 
						new Date())
				);
		transactions.add(
				new Transaction(
						110425, 
						"INSP", 
						new Account(
								"INVASSET", 
								"INVASSET", 
								1000, 
								0),
						new Account(
								"INVOICE", 
								"INVOICE", 
								0, 
								1000), 
						new Date())
				);
		transactions.add(
				new Transaction(
						110429, 
						"ISSUE", 
						new Account(
								"EXPENSE", 
								"EXPENSE", 
								100, 
								0),
						new Account(
								"INVASSET", 
								"INVASSET", 
								0, 
								100), 
						new Date())
				);
		producer.sendMessage(transactions);
	}*/
}
