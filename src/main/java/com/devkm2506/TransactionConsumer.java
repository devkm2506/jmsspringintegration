package com.devkm2506;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.support.JmsGatewaySupport;
import org.springframework.stereotype.Component;

@Component
public class TransactionConsumer extends JmsGatewaySupport {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public Transaction consumeMessage() {
		return (Transaction) jmsTemplate.receiveAndConvert();
	}

}
