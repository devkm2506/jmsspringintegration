package com.devkm2506;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.support.JmsGatewaySupport;
import org.springframework.stereotype.Component;

@Component
public class TransactionProducer extends JmsGatewaySupport {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	/*
	public void sendMessage(final String transaction) {
		jmsTemplate.convertAndSend(transaction);
	}*/
	
	public void sendMessage(final String message) {
		jmsTemplate.convertAndSend(message);
	}

}
