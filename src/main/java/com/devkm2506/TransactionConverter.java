package com.devkm2506;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

@Component
public class TransactionConverter implements MessageConverter {

	@Override
	public Message toMessage(Object object, Session session) throws JMSException, MessageConversionException {
		Transaction transaction = (Transaction)	object;
		Account firstAccount = transaction.getFirsttAccount();
		Account secondAccount = transaction.getSecondAccount();
		
		MapMessage message = session.createMapMessage();
		message.setInt("transactionId", transaction.getAccountId());
		message.setString("transactionType", transaction.getType());
		message.setString("firstAccountCode", firstAccount.getCode());
		message.setString("firstAccountType", firstAccount.getType());
		message.setFloat("firstDebit", firstAccount.getDebit());
		message.setFloat("firstCredit", firstAccount.getCredit());
		message.setString("secondAccountCode", secondAccount.getCode());
		message.setString("secondAccountType", secondAccount.getType());
		message.setFloat("secondDebit", secondAccount.getDebit());
		message.setFloat("secondCredit", secondAccount.getCredit());
		message.setString("dateString", transaction.getFormattedDate());
		
		return message;
	}

	@Override
	public Object fromMessage(Message message) throws JMSException, MessageConversionException {
		MapMessage mapMessage = (MapMessage) message;
		Object transaction = new Object();
		try {
			 transaction = new Transaction(
					mapMessage.getInt("transactionId"),
					mapMessage.getString("transactionType"),
					new Account(
							mapMessage.getString("firstAccountCode"), 
							mapMessage.getString("firstAccountType"), 
							mapMessage.getFloat("firstDebit"), 
							mapMessage.getFloat("firstCredit")),
					new Account(
							mapMessage.getString("secondAccountCode"),
							mapMessage.getString("secondAccountType"),
							mapMessage.getFloat("secondDebit"),
							mapMessage.getFloat("secondCredit")),
					new SimpleDateFormat(
							Transaction.DATE_FORMAT, 
							Locale.ENGLISH)
					.parse(mapMessage.getString("dateString")));
		} 
		catch (ParseException e) {
				System.out.println(e.toString());
		}
		return transaction;
	}

}
