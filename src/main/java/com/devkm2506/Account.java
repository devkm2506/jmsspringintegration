package com.devkm2506;

import java.util.Locale;

public class Account {
	
	private static final String DEBIT_AND_CREDIT_FORMAT = "%2.02f";
	
	private String accountCode;
	private String accountType;
	private float debit;
	private float credit;

	public String getDebitInValidFormat() {
		return String.format(new Locale("en", "EN"), DEBIT_AND_CREDIT_FORMAT, getDebit());
	}
	
	public String getCreditInValidFormat() {
		return String.format(new Locale("en", "EN"), DEBIT_AND_CREDIT_FORMAT, getCredit());
	}
	
	
	public Account(String code, String type, float debit, float credit){
		this.accountCode = code;
		this.accountType = type;
		this.debit = debit;
		this.credit = credit;
	}

	public String getCode() {
		return accountCode;
	}

	public String getType() {
		return accountType;
	}

	public float getDebit() {
		return debit;
	}

	public float getCredit() {
		return credit;
	}

}
