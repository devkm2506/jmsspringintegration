package com.devkm2506;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class Transaction {

	private final transient Account firstAccount;
	private final transient Account secondAccount;
	private final String transactionType;
	private final int transactionId;
	private final Date transactionDate;

	public static final String DATE_FORMAT = "YYYY-MM-dd HH:mm:ss";

    public Transaction(final int newAccountId, final String newType,
                       final Account debitAccount, final Account creditAccount,
                       final Date newDate) {
		this.transactionType = newType;
		this.firstAccount = debitAccount;
		this.secondAccount = creditAccount;
		this.transactionDate = newDate;
        this.transactionId = newAccountId;
    }

	protected final Account getFirsttAccount() {
		return this.firstAccount;
	}

	protected final Account getSecondAccount() {
		return this.secondAccount;
	}

	protected final String getType() {
		return this.transactionType;
	}

	protected final int getAccountId() {
		return this.transactionId;
	}

	protected final Date getDate() {
		return this.transactionDate;
	}
    
	protected final String getFormattedDate() {
        synchronized (this) {
            return new SimpleDateFormat(
                    DATE_FORMAT,
                    new Locale("en", "EN")
            ).format(this.transactionDate);
        }
	}
	
    public String toString() {
    	String[] args = {
				String.valueOf(this.transactionId), 
				this.transactionType, 
				this.firstAccount.getCode(), 
				this.firstAccount.getDebitInValidFormat(),
				this.firstAccount.getCreditInValidFormat(),
				this.secondAccount.getCode(),
				this.secondAccount.getDebitInValidFormat(),
				this.secondAccount.getCreditInValidFormat(),
				this.getFormattedDate()};
    	return String.format(
    			"Id: %s - Type: %s - Account: %s with %s debit/%s credit - Account: %s with %s debit/%s credit - %s",
    			(Object[])args);
    }
}
