package com.devkm2506.acceptance;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.spring.SpringStepsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public abstract class SpringJbehaveStory extends JUnitStory {
	
	public SpringJbehaveStory() {
		Embedder embedder = configuredEmbedder();
		embedder.embedderControls().doVerboseFailures(true).useStoryTimeouts("30");
		useEmbedder(embedder);
	}
	
	public Configuration configuration() {
		return new MostUsefulConfiguration()
				.useStoryReporterBuilder(
						new StoryReporterBuilder()
						.withDefaultFormats()
						.withFormats(Format.CONSOLE, Format.HTML, Format.TXT)
						.withCodeLocation(CodeLocations.codeLocationFromPath("build/jbehave"))
						);
	}
	
	@Override
	public InjectableStepsFactory stepsFactory() {
		return new SpringStepsFactory(configuration(), new ClassPathXmlApplicationContext("jmsContext.xml"));
	}

}
