package com.devkm2506.acceptance.stories;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.devkm2506.acceptance.AcceptanceTest;
import com.devkm2506.acceptance.SpringJbehaveStory;
import com.devkm2506.acceptance.steps.TransactionSteps;

@RunWith(SpringJUnit4ClassRunner.class)
@AcceptanceTest
public class TransactionStory extends SpringJbehaveStory {
}
