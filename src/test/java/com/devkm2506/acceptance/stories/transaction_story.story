Sample story

Narrative:
In order to use jbehave for acceptance tests
As a developer
I want to run a sample story
					 
Scenario:  A transaction is created
Given a transactionId of 110424, transactionType of "ADJPRC", 
firstAccountCode of "ADJPRC", firstAccountType of "ADJPRC",
firstAccountDebit of 0, firstAccountCredit of 0, 
secondAccountCode of "INVASSET", secondAccountType of "INVASSET",
secondAccountDebit of 0, secondAccountCredit of 0 and 
transactionDate of 26-04-2016 04:06:00
When a new transaction is created
Then the new transaction should have the same transactionId, transactionType, 
firstAccountCode, firstAccountType,firstAccountDebit, firstAccountCredit, 
secondAccountCode, secondAccountType, secondAccountDebit, secondAccountCredit 
and transactionDate