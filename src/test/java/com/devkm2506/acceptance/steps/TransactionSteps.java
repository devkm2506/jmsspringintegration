package com.devkm2506.acceptance.steps;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.devkm2506.Account;
import com.devkm2506.Transaction;
import com.devkm2506.acceptance.Steps;

@Steps 
public class TransactionSteps {
	
	private Transaction transaction;
	
	private int transactionId;
	private String transactionType;
	private Account firstAccount;
	private Account secondAccount;
	private Date transactionDate;
	
	@Given("a transactionId of $transactionId, transactionType of $transactionType, "
			+ "firstAccountCode of $firstAccountCode, firstAccountType of $firstAccountType, "
			+ "firstAccountDebit of $firstAccountDebit, firstAccountCredit of $firstAccountCredit, "
			+ "secondAccountCode of $secondAccountCode, secondAccountType of $secondAccountType, "
			+ "secondAccountDebit of $secondAccountDebit, secondAccountCredit of $secondAccountCredit "
			+ "and transactionDate of $transactionDate")
	public void readTransactionInformation(int transactionId, String transactionType,
			String firstAccountCode, String firstAccountType, float firstAccountDebit,
			float firstAccountCredit, String secondAccountCode, String secondAccountType, 
			float secondAccountDebit, float secondAccountCredit, Date transactionDate) {
		this.transactionId = transactionId;
		this.transactionType = transactionType;
		this.firstAccount = new Account(
				firstAccountCode, 
				firstAccountType, 
				firstAccountDebit, 
				firstAccountCredit);
		this.secondAccount = new Account(
				secondAccountCode, 
				secondAccountType, 
				secondAccountDebit, 
				secondAccountCredit);
		this.transactionDate = transactionDate;
	}
	
	@When("a new transaction is created")
	public void createTransaction() {
			this.transaction = new Transaction(
					this.transactionId, 
					this.transactionType, 
					this.firstAccount,
					this.secondAccount,
					this.transactionDate);
	}
	
	@Then("the new transaction should have the same transactionId, transactionType, "
			+ "firstAccountCode, firstAccountType,firstAccountDebit, firstAccountCredit, "
			+ "secondAccountCode, secondAccountType, secondAccountDebit, secondAccountCredit "
			+ "and transactionDate")
	public void checkTransactionFields() {
		assertThat(this.transaction, is(instanceOf(Transaction.class)));
	}
	
	@AsParameterConverter
	public Date createDate(String date) {
		Date convertedDate = null;
		try {
			convertedDate = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss", Locale.ENGLISH).parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return convertedDate;
	}
}
