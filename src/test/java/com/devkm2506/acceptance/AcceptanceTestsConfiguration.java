package com.devkm2506.acceptance;

import org.jbehave.core.annotations.spring.UsingSpring;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:jmsContext.xml")
@ComponentScan
public class AcceptanceTestsConfiguration {

}
