package com.devkm2506.acceptance;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ContextConfiguration(classes = AcceptanceTestsConfiguration.class)
@ImportResource({"classpath:/application.properties", "classpath:/tests.properties"})
@ActiveProfiles("tests")
@DirtiesContext
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AcceptanceTest {
	
	/*public AcceptanceTest() {
		Embedder embedder = configuredEmbedder();
		embedder.embedderControls().doVerboseFailures(true).useStoryTimeouts("30");
	}
	
	public Configuration configuration() {
		return new MostUsefulConfiguration()
				.useStoryReporterBuilder(
						new StoryReporterBuilder()
						.withDefaultFormats()
						.withFormats(Format.CONSOLE, Format.HTML, Format.TXT)
						.withCodeLocation(CodeLocations.codeLocationFromPath("build/jbehave"))
						);
	}
*/
}
