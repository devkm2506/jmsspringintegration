package com.devkm2506.unit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.devkm2506.Account;

import static org.testng.Assert.*;

public class AccountTestSuite {
	
	Account account;
	
	@BeforeMethod
	public void setUp(){
		String code = "ADJPRICE";
		String type = "ADJPRICE";
		float debit = 0;
		float credit = 0;
		
		account = new Account(code, type, debit, credit);
	}
	
	@AfterMethod
	public void tearDown(){
		this.account = null;
	}
	
	@Test
	public void TestAccountClass_ValidClass_ReturnsTrue(){
		assertTrue(this.account instanceof Account);
	}
	
	@Test
	public void TestAccountInstance_ValidInstance_ReturnsNotNull(){
		assertNotNull(this.account);
	}
	
	@Test
	public void TestAccountInstance_ValidFields_ReturnsNotNull(){
		assertNotNull(this.account.getCode());
		assertNotNull(this.account.getType());
		assertNotNull(this.account.getDebitInValidFormat());
		assertNotNull(this.account.getCreditInValidFormat());
	}
	
	@Test
	public void TestAccountInstance_ValidFields_ReturnsTrue(){		
		assertTrue(this.account.getCode().length() <= 10);
		assertTrue(this.account.getType().length() <= 10);
		assertTrue(this.account.getDebitInValidFormat().equals("0.00"));
		assertTrue(this.account.getCreditInValidFormat().equals("0.00"));
	}

}
