package com.devkm2506.unit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.devkm2506.Account;
import com.devkm2506.Transaction;

import static org.testng.Assert.*;

import java.util.Calendar;

public class TransactionTestSuite {
	
	private Transaction transaction;
	Account debitAccount;
	Account creditAccount;
	
	@BeforeMethod
	public void setUp(){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2016);
		calendar.set(Calendar.MONTH, Calendar.APRIL);
		calendar.set(Calendar.DAY_OF_MONTH, 13);
		calendar.set(Calendar.HOUR_OF_DAY, 9);
		calendar.set(Calendar.MINUTE, 40);
		calendar.set(Calendar.SECOND, 3);
		
		this.transaction = new Transaction(
				110423,
				"ADJPRICE",
				new Account("ADJPRICE", "ADJPRICE", 500, 0),
				new Account("INVASSET", "INVASSET", 0, 500),
				calendar.getTime()
				);
	}
	
	@AfterMethod
	public void tearDown(){
		this.transaction = null;
	}

	@Test
	public void TestTransactionInstance_NotNullInstance(){
		assertNotNull(this.transaction);
	}
	
}
